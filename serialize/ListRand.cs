﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;

namespace serialize
{
    class ListRand
    {
        public ListNode head;
        public ListNode tail;
        public int count;
        private List<ListNode> list = new List<ListNode>();

        public ListRand(int count)
        {
            for (int i = 1; i <= count; i++)
            {
                Add(Console.ReadLine());
            }

            // Добавляю случайные элементы в ноды
            ListNode node = head;
            Random rand = new Random();
            while (node != null)
            {
                node.Rand = FindNode(rand.Next(0, count - 1));
                node = node.Next;
            }
        }


        public void Serialize(FileStream s)
        {
            StreamWriter sw = new StreamWriter(s);
            ListNode[] ListNodeArray = ToArray();
            for (var i = 0; i < count; i++)
            {
                object[] values = {
                    Array.IndexOf(ListNodeArray, ListNodeArray[i].Rand),
                    ListNodeArray[i].Data
                };

                sw.WriteLine(String.Join(";", values));
            }
            sw.Close();
        }

        public void Deserialize(FileStream s)
        {
            ArrayList lineArray = new ArrayList();
            StreamReader sr = new StreamReader(s);

            while (!sr.EndOfStream)
            {
                lineArray.Add(sr.ReadLine());
            }
            sr.Close();

            String[][] propertiesArray = new String[lineArray.Count][];

            for (int i = 0; i < lineArray.Count; i++)
            {
                propertiesArray[i] = ((String)lineArray[i]).Split(';');
                Add(propertiesArray[i][1]);
            }

            for (int i = 0; i < count; i++)
            {
                FindNode(i).Rand = FindNode(Convert.ToInt32(propertiesArray[i][0]));
            }
        }



        /// <summary>
        /// Найдет элемент по индексу
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public ListNode FindNode(int index)
        {
            ListNode node = head;
            int i = 0;
            while (node != null && i < index)
            {
                node = node.Next;
                i++;
            }

            return node;
        }

        /// <summary>
        /// Добавит элемент в лист
        /// </summary>
        /// <param name="data"></param>
        public void Add(String data)
        {
            ListNode node = new ListNode(data);

            if (head == null)
                head = node;
            else
            {
                tail.Next = node;
                node.Prev = tail;
            }
            tail = node;
            count++;
        }

        /// <summary>
        /// отчистка списка
        /// </summary>
        public void Clear()
        {
            head = null;
            tail = null;
            count = 0;
        }

        /// <summary>
        /// Запишет элементы списка в массив
        /// </summary>
        /// <returns></returns>
        private ListNode[] ToArray()
        {
            ListNode[] ListNodeArray = new ListNode[count];
            ListNode node = head;
            int i = 0;
            while (node != null)
            {
                ListNodeArray[i] = node;
                node = node.Next;
                i++;
            }
            return ListNodeArray;
        }

        /// <summary>
        /// Вывод значений в консоль
        /// </summary>
        public void Write()
        {
            ListNode node = head;
            int i = 0;

            Console.WriteLine("\n");
            while (node != null)
            {
                Console.WriteLine(String.Format("Элемент{0}; Текст: {1}; Текст рандомного элемента: {2}", i, node.Data, node.Rand?.Data));
                node = node.Next;
                i++;
            }
        }
    }
}
