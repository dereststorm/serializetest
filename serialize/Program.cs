﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace serialize
{
    class Program
    {
        static void Main(string[] args)
        {

            ListRand list = new ListRand(4);
            list.Write();

            list.Serialize(new FileStream(@"../../text.txt", FileMode.Create, FileAccess.Write));
            list.Clear();

            list.Deserialize(new FileStream(@"../../text.txt", FileMode.OpenOrCreate, FileAccess.Read));
            list.Write();
            Console.ReadKey();
        }
    }
}
